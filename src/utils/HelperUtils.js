const formatAddress = (address) => {
  try {
    return address.toLowerCase();
  } catch (err) {
    console.log(err);
    return address;
  }
};

const compareAddress = (address1, address2) => {
  try {
    return formatAddress(address1) === formatAddress(address2);
  } catch (err) {
    return false;
  }
};

const exportObject = {
  formatAddress,
  compareAddress,
};
export default exportObject;
