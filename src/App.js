import './App.css';

import { useEffect, useState } from 'react';

import HelperUtils from '../src/utils/HelperUtils';
import NewMusicList from './components/NewMusicList';
import Web3 from 'web3';

function App() {
  const [web3, setWeb3] = useState(null);
  //const [accounts, setAccounts] = useState(null);
  const [account, setAccount] = useState(null);
  const [balance, setAccountBalance] = useState(0);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function loadBlockchain() {
      const loadAccount = (web3, accounts) => {
        const address = HelperUtils.formatAddress(accounts[0]);
        setAccount(address);
        web3.eth
          .getBalance(address)
          .then((balance) => {
            setAccountBalance(
              parseFloat(web3.utils.fromWei(balance)).toFixed(2)
            );
          })
          .catch((err) => {});
      };

      try {
        setLoading(true);
        const web3 = new Web3('http://localhost:7545');
        const accounts = await web3.eth.getAccounts();

        if (accounts.length === 0) {
          const error = 'No accounts configured';
          alert(error);
          throw new Error(error);
        }

        if (typeof window.ethereum === 'undefined') {
          const error = 'No metamask installed';
          alert(error);
          throw new Error(error);
        }

        setWeb3(web3);
        /*setAccounts(
          accounts.map((account) => HelperUtils.formatAddress(account))
        );*/
        const currentUserAccounts = await window.ethereum.request({
          method: 'eth_requestAccounts',
        });
        window.ethereum.on('accountsChanged', (currentUserAccounts) => {
          loadAccount(web3, currentUserAccounts);
        });
        loadAccount(web3, currentUserAccounts);
        setLoading(false);
      } catch (err) {
        console.log(err);
        setWeb3(null);
        //setAccounts(null);
        setAccount(null);
        setAccountBalance(0);
        setLoading(false);
      }
    }
    loadBlockchain();
  }, []);

  return (
    <div>
      <nav
        className='navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-10 shadow'
        style={{ position: 'sticky' }}
      >
        <div
          className='col-6'
          style={{
            color: '#fff',
            textAlign: 'start',
            paddingLeft: 5,
          }}
          target='_blank'
        >
          Music Store App
        </div>
      </nav>
      <div>
        <div className='col-12 d-flex justify-content-center'>
          {loading ? (
            <div id='loader' className='text-center'>
              <p className='text-center'>Loading...</p>
            </div>
          ) : (
            <>
              {web3 && account && (
                <div style={{ padding: 3, width: '100%' }}>
                  <div>
                    <p
                      style={{
                        fontSize: 20,
                      }}
                    >{`Welcome back ${account} !!!`}</p>
                    <p
                      style={{
                        fontSize: 20,
                      }}
                    >{`Balance ${balance} ETH`}</p>
                    <NewMusicList web3={web3} account={account} />
                  </div>
                </div>
              )}
            </>
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
