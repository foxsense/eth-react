import Button from 'react-bootstrap/Button';
import HelperUtils from '../../utils/HelperUtils';
import React from 'react';
import Table from 'react-bootstrap/Table';

const OwnedMusicCard = (props) => {
  return (
    <div
      style={{
        border: 'solid',
        borderColor: 'black',
        borderRadius: 5,
        borderWidth: 2,
        padding: 5,
        height: 390,
        ...props.style,
      }}
    >
      <Table bordered hover style={{ height: 'auto' }}>
        <tbody>
          <tr>
            <td>Album </td>
            <td>{props.music.data.name}</td>
          </tr>
          <tr>
            <td>Price </td>
            <td>{`${props.music.data.price} ETH`}</td>
          </tr>
          {props.music.data.isSold ? (
            <tr>
              <td>Sold To </td>
              <td>{props.music.data.soldTo}</td>
            </tr>
          ) : (
            <tr>
              <td>Sold To </td>
              <td></td>
            </tr>
          )}
        </tbody>
      </Table>
      {props.music.data.bids.length > 0 ? (
        <Table bordered hover style={{ marginTop: 5 }}>
          <tbody
            style={{ display: 'block', maxHeight: 250, overflowY: 'auto' }}
          >
            {props.music.data.bids.map((bid) => {
              return (
                <tr key={bid.bidId} style={{ display: 'table', width: '100%' }}>
                  <td style={{ overflowWrap: 'anywhere' }}>{bid.biddedBy}</td>
                  <td>{`${bid.amount} ETH`}</td>
                  <td>
                    {!props.music.data.isSold ? (
                      <Button
                        onClick={() =>
                          props.claimBid(props.music.tokenId, bid.bidId)
                        }
                      >
                        CLAIM
                      </Button>
                    ) : (
                      <p>
                        {HelperUtils.compareAddress(
                          bid.biddedBy,
                          props.music.data.soldTo
                        )
                          ? 'WON !!!!!'
                          : 'Oops !!'}
                      </p>
                    )}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      ) : (
        <div style={{ textAlign: 'center', marginTop: 5 }}>No bids !!</div>
      )}
    </div>
  );
};

export default OwnedMusicCard;
