import React, { useState } from 'react';

import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';

const AvailableMusicCard = (props) => {
  const [bidAmount, setBidAmount] = useState(props.music.data.price);

  const bid = () => {
    if (bidAmount > props.music.data.price)
      props.bidForMusic(props.music.tokenId, bidAmount);
  };

  return (
    <div
      style={{
        border: 'solid',
        borderColor: 'black',
        borderRadius: 5,
        borderWidth: 2,
        padding: 5,
        height: 310,
        ...props.style,
      }}
    >
      <Table bordered hover>
        <tbody>
          <tr>
            <td>Album </td>
            <td>{props.music.data.name}</td>
          </tr>
          <tr>
            <td>Price </td>
            <td>{`${props.music.data.price} ETH`}</td>
          </tr>
          <tr>
            <td>Creator </td>
            <td style={{ overflowWrap: 'anywhere' }}>
              {props.music.data.owner}
            </td>
          </tr>
          <tr>
            <td>Bid Amount </td>
            <td>
              <input
                style={{ width: 100 }}
                onChange={(event) => {
                  const value = Number(event.currentTarget.value);
                  if (value > props.music.data.price) setBidAmount(value);
                  else setBidAmount(props.music.data.price);
                }}
                value={bidAmount}
                type='number'
                className='form-control'
                min={props.music.data.price}
                placeholder='e.g. 100'
              />
            </td>
          </tr>
        </tbody>
      </Table>
      <div style={{ display: 'flex', marginTop: 25 }}>
        <Button style={{ margin: 'auto' }} onClick={bid}>
          BID
        </Button>
      </div>
    </div>
  );
};

export default AvailableMusicCard;
