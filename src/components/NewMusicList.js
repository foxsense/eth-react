import React, { useEffect, useState } from 'react';

import AvailableMusic from './newMusicList/AvailableMusic';
import HelperUtils from '../utils/HelperUtils';
import MusicContract from '../contracts/MusicContract';
import OwnedMusic from './newMusicList/OwnedMusic';

const NewMusicList = (props) => {
  const [contract, setContract] = useState(null);
  const [ownedMusicList, setOwnedMusicList] = useState(null);
  const [availableMusicList, setAvailableMusicList] = useState(null);

  useEffect(() => {
    const contract = new props.web3.eth.Contract(
      MusicContract.MUSIC_ABI,
      MusicContract.MUSIC_ADDRESS
    );
    setContract(contract);
  }, [props.web3]);

  useEffect(() => {
    loadMusicData(contract);
  }, [props.account, contract]);

  const loadMusicData = async (contract) => {
    if (!contract) return;
    try {
      const count = await contract.methods.totalSupply().call();
      const availableMusic = [];
      const ownedMusic = [];

      for (let index = 0; index < count; index++) {
        const music = await contract.methods.getMusic(index).call();
        const owner = HelperUtils.formatAddress(
          await contract.methods.ownerOf(index).call()
        );
        const tokenId = Number(music[0]);
        const name = music[1];
        const price = Number(music[2]);
        const isSold = music[3];
        const soldTo = HelperUtils.formatAddress(music[4]);
        const soldAmount = Number(music[5]);
        if (HelperUtils.compareAddress(props.account, owner)) {
          const bids = [];
          const bidsCount = await contract.methods.totalBidsCount(index).call();
          for (let bidIndex = 0; bidIndex < bidsCount; bidIndex++) {
            const bid = await contract.methods.getBid(index, bidIndex).call();
            bids.push({
              bidId: Number(bid[0]),
              biddedBy: HelperUtils.formatAddress(bid[1]),
              amount: Number(bid[2]),
            });
          }
          ownedMusic.push({
            tokenId,
            data: {
              name,
              price,
              isSold,
              soldTo,
              soldAmount,
              owner,
              bids,
            },
          });
        } else if (!isSold) {
          availableMusic.push({
            tokenId,
            data: {
              name,
              price,
              isSold,
              soldTo,
              soldAmount,
              owner,
            },
          });
        }
      }
      setOwnedMusicList(ownedMusic);
      setAvailableMusicList(availableMusic);
    } catch (err) {
      console.log(err);
      setAvailableMusicList(null);
      setOwnedMusicList(null);
    }
  };

  const createNewMusic = (musicName, price) => {
    contract.methods
      .mint(musicName, price)
      .send({ from: props.account, gas: 1000000 })
      .once('receipt', (receipt) => {
        console.log(receipt);
        loadMusicData(contract);
      })
      .once('error', (error) => {
        console.log(error);
      });
  };

  const bidForMusic = (musicTokenId, bidAmount) => {
    try {
      window.ethereum
        .request({
          method: 'eth_sendTransaction',
          params: [
            {
              from: props.account,
              to: MusicContract.MUSIC_ADDRESS,
              value: props.web3.utils.toHex(
                props.web3.utils.toWei(String(bidAmount), 'ether')
              ),
            },
          ],
        })
        .then((result) => {
          console.log(result);
          contract.methods
            .bid(musicTokenId, bidAmount)
            .send({ from: props.account, gas: 1000000 })
            .once('receipt', (receipt) => {
              console.log(receipt);
              loadMusicData(contract);
            })
            .once('error', (error) => {
              console.log(error);
            });
        })
        .catch((err) => {
          alert(err.message);
        });
    } catch (err) {
      alert(err.message);
    }
  };

  const claimBid = (musicTokenId, bidTokenId) => {
    try {
      contract.methods
        .claim(musicTokenId, bidTokenId)
        .send({ from: props.account, gas: 1000000 })
        .once('receipt', (receipt) => {
          console.log(receipt);
          loadMusicData(contract);
        })
        .once('error', (error) => {
          console.log(error);
        });
    } catch (err) {
      alert(err.message);
    }
  };

  return (
    <div>
      <OwnedMusic
        createMusic={createNewMusic}
        claimBid={claimBid}
        ownedMusicList={ownedMusicList}
      />
      <hr style={{ marginRight: 5, marginLeft: 5 }} />
      <AvailableMusic
        availableMusicList={availableMusicList}
        bidForMusic={bidForMusic}
      />
    </div>
  );
};

export default NewMusicList;
