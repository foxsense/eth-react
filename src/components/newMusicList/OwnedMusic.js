import React, { useState } from 'react';

import Button from 'react-bootstrap/Button';
import OwnedMusicCard from '../common/OwnedMusicCard';

const OwnedMusic = (props) => {
  const [musicName, setMusicName] = useState('');
  const [musicPrice, setMusicPrice] = useState(0);

  return (
    <div
      style={{
        border: 'solid',
        borderColor: 'black',
        borderRadius: 5,
        borderWidth: 2,
        padding: 10,
      }}
    >
      <h1>Owned Music</h1>
      <form
        onSubmit={(event) => {
          event.preventDefault();
          if (musicPrice > 0) {
            props.createMusic(musicName, musicPrice);
            setMusicName('');
            setMusicPrice(0);
          }
        }}
        style={{ padding: 10, display: 'flex' }}
      >
        <input
          style={{ marginRight: 15, width: 300 }}
          id='musicName'
          onChange={(event) => {
            setMusicName(event.currentTarget.value);
          }}
          value={musicName}
          type='text'
          className='form-control'
          placeholder='e.g. Bang Bang'
          required
        />
        <input
          style={{ marginRight: 15, width: 300 }}
          id='musicPrice'
          onChange={(event) => {
            const value = Number(event.currentTarget.value);
            if (value > 0) setMusicPrice(value);
            else setMusicPrice(0);
          }}
          value={musicPrice}
          type='number'
          className='form-control'
          placeholder='e.g. 100'
          required
        />
        <Button style={{ marginRight: 5, width: 100 }} type='submit'>
          CREATE
        </Button>
      </form>
      <hr style={{ marginLeft: 10, marginRight: 10 }} />
      {props.ownedMusicList && props.ownedMusicList.length > 0 ? (
        <div style={{ width: '100%' }}>
          {props.ownedMusicList.map((music) => {
            return (
              <OwnedMusicCard
                key={music.tokenId}
                style={{ margin: 10 }}
                music={music}
                claimBid={props.claimBid}
              />
            );
          })}
        </div>
      ) : (
        <div style={{ textAlign: 'center' }}>No owned music</div>
      )}
    </div>
  );
};

export default OwnedMusic;
