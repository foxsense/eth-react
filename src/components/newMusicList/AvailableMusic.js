import AvailableMusicCard from '../common/AvailableMusicCard';
import React from 'react';

const AvailableMusic = (props) => {
  return (
    <div
      style={{
        border: 'solid',
        borderColor: 'black',
        borderRadius: 5,
        borderWidth: 2,
        padding: 10,
      }}
    >
      <h1>Available Music</h1>
      {props.availableMusicList && props.availableMusicList.length > 0 ? (
        <div style={{ width: '100%' }}>
          {props.availableMusicList.map((music) => {
            return (
              <AvailableMusicCard
                key={music.tokenId}
                style={{ width: '48%', margin: 10, display: 'inline-block' }}
                music={music}
                bidForMusic={props.bidForMusic}
              />
            );
          })}
        </div>
      ) : (
        <div style={{ textAlign: 'center' }}>No available music</div>
      )}
    </div>
  );
};

export default AvailableMusic;
