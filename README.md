1. Install dependencies using **npm install**
2. Download Metamask Chrome extension
3. Start the Ganache server at any port (eg: http://localhost:7545)
4. Connect the Web3 to the above Ganache local blockchain
